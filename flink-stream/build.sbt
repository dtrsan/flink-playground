name := "flink-stream"

version := "1.0"

scalaVersion := "2.11.11"

fork in run := true

val flinkVersion = "1.2.1"

libraryDependencies ++= Seq(
  "org.apache.flink" %% "flink-scala" % flinkVersion % "provided",
  "org.apache.flink" %% "flink-streaming-scala" % flinkVersion % "provided",
  "org.apache.flink" %% "flink-connector-kafka-0.10" % flinkVersion,
  "org.scalatest" %% "scalatest" % "3.0.1" % "test"
)

mainClass in assembly := Some("playground.flink.stream.Main")

// make run command include the provided dependencies
run in Compile := Defaults.runTask(fullClasspath in Compile, mainClass in (Compile, run), runner in (Compile, run))

// exclude Scala library from assembly
assemblyOption in assembly := (assemblyOption in assembly).value.copy(includeScala = false)
