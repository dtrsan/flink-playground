package playground.flink.stream

import java.util.Properties

import org.apache.flink.api.common.functions.MapFunction
import org.apache.flink.api.common.typeinfo.TypeInformation
import org.apache.flink.streaming.api.scala.{DataStream, StreamExecutionEnvironment}
import org.apache.flink.streaming.connectors.kafka.{FlinkKafkaConsumer010, FlinkKafkaProducer010}
import org.apache.flink.streaming.util.serialization.SimpleStringSchema

/*
Useful kafka commands
./kafka-topics.sh --create --zookeeper localhost:2181 --topic flink-stream-input --partitions 12 --replication-factor 1
./kafka-topics.sh --create --zookeeper localhost:2181 --topic flink-stream-output --partitions 12 --replication-factor 1

./kafka-console-producer.sh --broker-list localhost:9092 --topic flink-stream-input
./kafka-console-consumer.sh --zookeeper localhost:2181 --topic flink-stream-output

./kafka-consumer-offset-checker.sh --zookeeper localhost:2181 --topic flink-stream-input --group flink-stream-group
 */
object Main {

  def mapper = new MapFunction[String, String] {
    override def map(value: String): String = value.toUpperCase
  }

  def main(args: Array[String]): Unit = {
    val env = StreamExecutionEnvironment.getExecutionEnvironment

    implicit val typeInfo = TypeInformation.of(classOf[String])
    val stream: DataStream[String] = env.addSource(kafkaConsumer)
    val processed = stream.map(mapper)
    kafkaSink(processed)
    env.execute("flink-stream")
  }

  def kafkaConsumer: FlinkKafkaConsumer010[String] = {
    val properties = new Properties()
    properties.setProperty("bootstrap.servers", "localhost:9092")
    properties.setProperty("group.id", "flink-stream-group")

    new FlinkKafkaConsumer010[String]("flink-stream-input", new SimpleStringSchema, properties)
  }

  def kafkaSink(input: DataStream[String]): Unit = {
    val properties = new Properties()
    properties.setProperty("bootstrap.servers", "localhost:9092")

    val producer = FlinkKafkaProducer010.writeToKafkaWithTimestamps(
      input.javaStream,
      "flink-stream-output",
      new SimpleStringSchema,
      properties
    )

    // the following is necessary for at-least-once delivery guarantee
    producer.setLogFailuresOnly(false)   // "false" by default
    producer.setFlushOnCheckpoint(true)  // "false" by default
  }
}
