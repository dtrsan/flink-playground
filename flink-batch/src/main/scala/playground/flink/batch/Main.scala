package playground.flink.batch

import org.apache.flink.api.java.aggregation.Aggregations
import org.apache.flink.api.scala.{DataSet, ExecutionEnvironment}
import org.apache.flink.api.scala.hadoop.mapred.HadoopOutputFormat
import org.apache.hadoop.fs.Path
import org.apache.hadoop.mapred.{FileOutputFormat, JobConf, TextOutputFormat}
import org.apache.flink.api.scala.createTypeInformation

object Main {

  val input = "hdfs://localhost:9000/flink-batch/input"
  val output = "hdfs://localhost:9000/flink-batch/output"

  def main(args: Array[String]): Unit = {

    val env = ExecutionEnvironment.getExecutionEnvironment
    val data = env.readTextFile(input)
    val step1: DataSet[(String, Int)] = data
      .map(_.toLowerCase)
      .map((_, 1))
      .groupBy(0)
      .aggregate(Aggregations.SUM, 1)

    val out = step1
    out.output(hadoopOutputFormat)

    env.execute("flink-batch")
  }

  def hadoopOutputFormat: HadoopOutputFormat[String, Int] = {
    val hadoopOF = new HadoopOutputFormat[String, Int](new TextOutputFormat[String, Int], new JobConf)
    FileOutputFormat.setOutputPath(hadoopOF.getJobConf, new Path(output))
    hadoopOF
  }
}
