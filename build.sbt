name := "flink-playground"

version := "1.0"

scalaVersion := "2.11.11"

lazy val root = Project(id = "flink-playground", base = file(".")) aggregate(flinkBatch, flinkStream)
  dependsOn(flinkBatch, flinkStream)

lazy val flinkBatch = Project(id = "flink-batch", base = file("flink-batch"))
lazy val flinkStream = Project(id = "flink-stream", base = file("flink-stream"))
