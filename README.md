# Apache Flink playground

## Building instructions

### Run tests
~~~shell
sbt test           # includes all projects
sbt $PROJECT/test  # run tests per project
~~~

### Measure test coverage
~~~shell
sbt clean coverage test coverageReport
~~~

### Build fat jar

~~~shell
sbt assembly           # includes all projects
sbt $PROJECT/assembly  # build fat jar per project
~~~

### Run project

~~~shell
java -classpath /path/to/flink-playground.jar package.Main
~~~

Example how to run flink-batch project
~~~shell
java -classpath /path/to/flink-playground-assembly-1.0.jar playground.flink.batch.Main
~~~

